import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import SignUp from './components/SignUp.vue'
import SignIn from './components/SignIn.vue'
import Home from './components/Home.vue'
import Index from './components/Index.vue'
import {store} from './store'

Vue.config.productionTip = false
Vue.use(VueRouter);

const routes = [
  {path:'/signup',component: SignUp},
  {path:'/signin',component: SignIn},
  {path: '/home',component: Home},
  { path:'/',component: Index},
];

const router = new VueRouter({
  routes
});

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
