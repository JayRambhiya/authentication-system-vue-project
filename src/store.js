 import Vue from 'vue';
 import Vuex from 'vuex';

 Vue.use(Vuex);

 export const store = new Vuex.Store({
     state:{
         users : [
            {'role':'Student','username': 'RajSharma','email': 'rajs@gmail.com','phoneNumber': 8844411122,'password':'RajS!4499'},
            {'role':'Teacher','username': 'AnitaSoman','email': 'anitas@gmail.com','phoneNumber': 9944433322,'password':'AnitaS@123'},
         ],
     }
 });